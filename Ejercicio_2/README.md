## Ejercicio 2: Crear un StatefulSet con 3 instancias de MongoDB. <br>
* #### Habilitar el clúster de MongoDB.
Para empezar con el clúster de MongoDB, voy a necesitar crear un statefulSet para crear replicas de MongoDB y mantener un estado consistente entre ellas. Por otro lado, va a ser también necesario la creación de un servicio "headless" que se va a encargar de que las distintas replicas en el cluster puedan identificarse entre ellas y mantener consistencia en los nombres DNS de los pods.<br>

Service: <br>
Para que el servicio sea headless necesitamos definirlo con el tipo ClusterIP y no asignar ninguna ClusterIP (ClusterIP: none). <br>
```
apiVersion: v1
kind: Service
metadata:
  name: mongo-service
  labels:
    app: mongo-service
    tier: Database
    version: v1.0.0
spec:
  ports:
  - name: mongo
    port: 27017
    targetPort: 27017
  clusterIP: None
  selector:
    app: mongo
    tier: Database
    version: v1.0.0
```

Creo el servicio mediante el comando:
```
$ kubectl apply -f headless-service.yaml
```

StatefulSet:
```
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: mongo
spec:
  selector:
    matchLabels:
      app: mongo-replica
  serviceName: "mongo-service"
  replicas: 3
  template:
    metadata:
      labels:
        app: mongo-replica
        tier: Database
        version: v1.0.0
    spec:
      terminationGracePeriodSeconds: 10
      containers:
      - name: mongo
        image: mongo:5.0
        command: 
        - mongod 
        - "--bind_ip_all"
        - "--replSet"
        - rs0
        ports:
        - containerPort: 27017
```

Para el statefulSet indico un periodo de gracia para finalizar los pods puesto que podrían haber connexiones activas a la base de datos cuando decidamos terminarla y quiero dar tiempo a que se cierren y se quede un estado consistente de la base de datos. <br>
Por otro lado, seteo algunas propiedades al proceso mongod mediante la ejecucion de comandos dentro del pod. El objetivo es permitir que los distintos nodos se puedan connectar entre ellos, puesto que por defecto solo són accesibles desde el localhost. <br>
A continuación indico el puerto que debe exponer el contenedor, este es el puerto por defecto de connexión con mongodb, el 27017. <br>

Creo el StatefulSet mediante el comando: <br>
```
$ kubectl apply -f statefulset.yaml
```
![Elementos desplegados en el cluster](.bin/img/ex_2-01.png) <br> <br>

Una vez hecho el deploy del StatefulSet de MongoDB, me dispongo a configurar la base de datos y comprobar que las distintas replicas se mantienen consistentes con la configuración aplicada a una de ellas. <br>
Empiezo conectandome al primero de los pods y ejecuto la shell de mongo mediante el comando:

```
$ kubectl exec -it mongo-0 -- mongo
```

Una vez dentro, inicializo la configuracion por defecto del replicaset de mongo mediante el comando:
```
> rs.initiate()
```
A continuación me dispongo a añadir los distintos miembros a la configuración. Para ello consideraré que la replica 0 de mongo es mi master y voy a configurarle su host para que sea accesible desde dentro del cluster. Para ello, en primer lugar creo una variable de configuración de replicaset de mongo:

```
> var cfg = rs.conf()
```

Seguidamente, configuro mi replica 0 con su host:
```
cfg.members[0].host="mongo-0.mongo-service:27017"
```
El host en este caso será <nombre_replica>.<nombre_servicio>:<numero_puerto> <br>

Aplico la configuración al replicaset de mongo mediante el comando:
```
> rs.reconfig(cfg)
```
De este modo queda definido el primer miembro de mi replicaSet de mongo. <br>

Compruebo la configuracion del replicaset de mongo mediante el comando:
```
> rs.status()
```
![Primera replica añadida](.bin/img/ex_2-02.png) <br>

A continuación me dispongo a añadir los 2 worker nodes como miembros del replicaset:

```
> rs.add("mongo-1.mongo-service:27017")
> rs.add("mongo-2.mongo-service:27017")
```
Vuelvo a comprobar los miembros del replicaset de mongo. Ahora me fijo en el parametro 'syncSourceHost' de las 2 replicas añadidas. <br>
![Replicas añadidas](.bin/img/ex_2-03.png) <br>

Como se puede comprobar, las replicas se están sincronizando entre ellas. El punto de partida de las sincronizaciones es la replica mongo-0.

* #### Realizar una operación en una de las instancias a nivel de configuración y verificar que el cambio se ha aplicado al resto de instancias. <br>

A continuación comprobaré que la sincronización entre el nodo master y los nodos worker de mongoDB funciona correctamente. Para ello voy a conectarme al primary node y crearé un usuario "testUser" y posteriormente me conectaré a uno de los worker nodes y comprobaré el listado de usuarios de la base de datos. <br>

![Creación de usuario](.bin/img/ex_2-04.png) <br>

Una vez creado el usuario me conecto a un worker node y compruebo si se ha sincronizado con el master. <br>

![Creación de usuario](.bin/img/ex_2-05.png) <br>

Efectivamente el worker node tiene dado de alta el usuario "testUser". Por lo tanto, se ha sincronizado correctamente con el master. <br>

* #### Diferencias que existiría si el montaje se hubiera realizado con el objeto de ReplicaSet <br>

Tal y como el nombre indica, el StatefulSet nos permite escalar y mantener un estado consistente entre las distintas replicas de una aplicación. El StatefulSet es deseable usarlo con aplicaciónes que guardan estado, como es el caso de una base de datos. Este nos proporciona un identificador en la network estable y único que se usa para descubrir otros miembros en el cluster. Las distintas replicas de un statefulset se sincronizan entre ellas, de modo que la informacion albergada en el nodo master se propaga hacia los worker nodes. La relación entre el master node y los distintos workers guarda también un orden que determina como se va a encender, apagar o substituir los distintos pods del set.
<br>
En caso de haber realizado el ejercicio con un ReplicaSet, dispondríamos de la capacidad de escalar las replicas de mongo pero no de mantener su consistencia, puesto que estas no se sincronizarían entre ellas. De modo que cuando realizaramos un cambio en la configuración de una, no se transmitiria a las otras, dejando el estado de la base de datos inconsistente. El replicaset tampoco mantiene un orden de encendido, apagado y substitución entre sus replicas. 