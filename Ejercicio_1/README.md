## Ejercicio 1 <br>

#### a. tras haber expuesto el servicio en el puerto 80, se deberá acceder a la página principal de Nginx a través de la siguiente URL: adriamanero.student.lasalle.com <br>

![Diagrama del ejercicio](.bin/img/ex_1-01.png) <br>

Para el desarrollo de una arquitectura como la de la imagen anterior y poder acceder al servidor nginx la url especificada, empiezo con el deploy de las replicas de nginx. Para ello me baso en un Deployment ya desarrollado en el anterior ejercicio de kubernetes, se encuentra en el fichero deployment.yaml: <br>

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deploy-v2
  labels:
    app: nginx-server
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx-server
      version: v2.0.0
  template:
    metadata:
      labels:
        app: nginx-server
        version: v2.0.0
    spec:
      containers:
      - name: nginx-container
        image: nginx:1.19.4
        ports:
        - containerPort: 80
        resources:
          limits:
            memory: "256Mi"
            cpu: "100m"
          requests:
            memory: "256Mi"
            cpu: "100m"
```

Ejecuto el deployment con el siguiente comando: <br>

```
$ kubectl apply -f deployment.yaml
```

![Deployment aplicado](.bin/img/ex_1-02.png) <br>

El resultado es un deployment de un replicaset con 3 replicas de nginx en la version 1.19.4. <br>

Seguidamente me dispongo a hacer deploy de un servicio para disponer de acceso a las replicas de nginx. El servicio es del tipo ClusterIP por el hecho que se trata de un servicio interno dentro del cluster. No quiero exponer directamente mi aplicación mediante el servicio, sino que quiero usar el servicio como puerta de enlace interna con mi aplicación. El encargado de exponer mi aplicación al exterior posteriormente será el Ingress Controller, el cual se conectará al servicio y este, a su vez, a la aplicación. <br>

```
apiVersion: v1
kind: Service
metadata:
  name: nginx-service
spec:
  type: ClusterIP
  selector:
    app: nginx-server
    version: v2.0.0
  ports:
  - port: 80
    targetPort: 80
```

Ejecuto el servicio con el siguiente comando: <br>

```
$ kubectl apply -f service.yaml
```

![Servicio aplicado](.bin/img/ex_1-03.png) <br>

A continuación me dispongo a implementar el ingress controller, este me permite gestionar accesos externos a los servicios de mi cluster mediante una url estática que yo defina. Es decir, ingres expone rutas HTTP y HTTPS al exterior del cluster que se mapean con servicios internos al cluster. Además permite controlar parte del enrutamiento del tráfico mediante la definicion de reglas en su fichero yaml. <br>
Mi objetivo será exponer la URL que se pide en el ejercicio y mapear la llamada a dicha URL con el servicio que acabo de crear <br>

En primer lugar será necesario habilitar la funcionalidad de ingress controllers en minikube mediante el siguiente comando: <br>

```
$ minikube addons enable ingress
```
![Enable ingress](.bin/img/ex_1-04.png) <br>

Posteriormente, la definición del Ingress: <br>

```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: nginx-ingress
  labels:
    name: nginx-ingress
    version: v2.0.0
spec:
  rules:
  - host: adriamanero.students.lasalle.com
    http:
      paths:
      - pathType: Exact
        path: "/"
        backend:
          service:
            name: nginx-service
            port: 
              number: 80  
```

Ejecuto el Ingress mediante el comando: <br>

```
$ kubectl apply -f ingress.yaml
```
![Ingress desplegado](.bin/img/ex_1-05.png) <br>

Como se puede ver en la captura, el Ingress tiene una dirección IP asignada. A continuación debo mapear la resolucion que hace mi ordenador de la URL adriamanero.student.lasalle.com a dicha IP. El motivo es que no existe ningun servidor DNS capaz de resolver dicha URL (puesto que es inventada), por lo tanto, para que el ordenador sepa mapear la URL a la IP del ingress controller, necesito definir dicha relacion en el fichero /etc/hosts. <br>

![Definir maping IP - URL](.bin/img/ex_1-06.png) <br>

Finalmente compruebo que dispongo de acceso a la web de bienvenida de Nginx mediante la url adriamanero.student.lasalle.com: <br>

![Se dispone de acceso a la URL](.bin/img/ex_1-07.png)

#### b. Una vez realizadas las pruebas con el protocolo HTTP, se pide acceder al servicio mediante la utilización del protocolo HTTPS.

Para securizar la comunicación con la pagina web mediante el protocolo HTTPS va a ser necesario generar mediante OpenSSL una clave privada y certificado autofirmado. Posteriormente voy a introducir la clave y el certificado en un secret de kubernetes. Finalmente configuraré el secret en el ingress controller de tal modo que se sirva como prueba de autenticidad de la web al ingresar en la URL adriamanero.students.lasalle.com.
<br>
Empiezo generando la clave. Para ello ejecuto:

```
$ openssl genrsa -out ca.key 2048
```

En segundo lugar me dispongo a generar el certificado autofirmado con la clave que acabo de generar:

```
openssl req -x509 -new -nodes -days 365 -key ca.key -out ca.crt -subj "/CN=adriamanero.students.lasalle.com"
```
mediante el flag "-days" especifico el periodo de validez del certificado. El flag "-key" me permite especificar la clave con la que se firmará el certificado. Finalmente, mediante el flag "-subj" especifico el "common name", es decir, el dns que ha de ser protegido por el certificado que estoy creando.
<br>
A continuación, una vez creados la clave y el certificado, me dispongo a crear el secret de kubernetes:

```
apiVersion: v1
kind: Secret
metadata:
  name:  nginx-tls-secret
data:
  tls.crt: LS0tLS1CRUdJTiBDRV...
  tls.key: LS0tLS1CRUdJTiBSU0EgUFJJVkF...
type: kubernetes.io/tls
```

Finalmente modifico el manifiesto del Ingress controller del apartado anterior para añadir la securización con TLS y especificar el secret:

```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: nginx-ingress
  labels:
    name: nginx-ingress
    version: v2.0.0
spec:
  tls:
    - hosts:
      - adriamanero.students.lasalle.com
      secretName: nginx-tls-secret
  rules:
  - host: adriamanero.students.lasalle.com
    http:
      paths:
      - pathType: Exact
        path: "/"
        backend:
          service:
            name: nginx-service
            port: 
              number: 80  
```

Ahora pruebo de acceder a la página web y el navegador me muestra que la página no es segura (por el hecho que el certificado introducido es autofirmado y el navegador no lo considera seguro): <br>

![Se dispone de acceso a la URL](.bin/img/ex_1-08.png) <br>

Por terminar, inspecciono el certificado de la web y compruebo que los detalles se corresponden con los que he introducido:

![Se dispone de acceso a la URL](.bin/img/ex_1-09.png) <br>

