## Ejercicio 3 
#### Crea un objeto de kubernetes HPA, que escale a partir de las métricas CPU o memoria (a vuestra elección). Establece el umbral al 50% de CPU/memoria utilizada, cuando pase el umbral, automáticamente se deberá escalar al doble de replicas. <br>

Para proceder con este ejercicio, en primer lugar he creado un Deployment de nginx con una sola replica. Ha sido necesario definir los resources del contenidor de nginx con el objetivo de permitir que kubernetes calcule el porcentaje de carga de las distintas replicas que se van a desplegar. <br>

```
apiVersion: autoscaling/v2beta2
kind: HorizontalPodAutoscaler
metadata:
  name: nginx-deploy-autoscaler
spec:
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: nginx-deploy-v2
  minReplicas: 1
  maxReplicas: 20
  metrics:
  - type: Resource
    resource:
      name: cpu
      target:
        type: Utilization
        averageUtilization: 50
  behavior:
    scaleUp:
      stabilizationWindowSeconds: 1
      policies:
      - type: Percent
        value: 100
        periodSeconds: 1
      selectPolicy: Max
```
En segundo lugar, he creado un servicio para poder lanzar peticiones a mi Deployment y así generar una carga de CPU por encima del 50% y ver como el Horizontal Pod Autoscaler entra en acción. El servicio es del tipo NodePort. <br>

```
apiVersion: v1
kind: Service
metadata:
  name: nginx-service
spec:
  type: NodePort
  selector:
    app: nginx-server
    version: v2.0.0
  ports:
  - port: 80
    targetPort: 80
```
En tercer y ultimo lugar, he creado el Horizontal Pod Autoscaler. Éste tiene que satisfacer ciertas condiciones que se especifican en el ejercicio. Estas son que el deployment escale el numero de replicas al doble en el momento que la carga de CPU llegue a más de un 50%. <br>
```
apiVersion: autoscaling/v2beta2
kind: HorizontalPodAutoscaler
metadata:
  name: nginx-deploy-autoscaler
spec:
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: nginx-deploy-v2
  minReplicas: 1
  maxReplicas: 20
  metrics:
  - type: Resource
    resource:
      name: cpu
      target:
        type: Utilization
        averageUtilization: 50
  behavior:
    scaleUp:
      stabilizationWindowSeconds: 1
      policies:
      - type: Percent
        value: 100
        periodSeconds: 1
      selectPolicy: Max
```
Para especificar la carga máxima de CPU del deployment he definido en el campo "metrics" un "Resource" con nombre "cpu", tipo "Utilization" y el parámetro "averageUtilization" igual a 50. <br>
Para especificar cuántas replicas se deben desplegar al superar la carga máxima he definido en el campo "behaviour" el comportamiento para la acción "scaleUp". Este consiste en una política de tipo "Percent" y un valor de "100". De este modo, cada vez que se llegue a la utilización maxima de CPU y el HPA vaya a escalar el deployment, va a desplegar un 100% más de replicas que las que haya en ese momento, duplicando por lo tanto el numero de replicas. <br>


